const fetch = require('node-fetch');
const moment = require('moment');

async function fetchGdxGames({ date }) {
  const m = moment(date)
  const year = m.format("YYYY")
  const month = m.format("MM")
  const day = m.format("DD")

  const url = `http://gdx.mlb.com/components/game/mlb/year_${year}/month_${month}/day_${day}/miniscoreboard.json`
  console.log('fetchGdxGames', year, month, day, url)

  const result = await fetch(url).then(res => res.json())

  const now = new Date()

  return result.data.games.game.map(doc => ({ updatedAt: now, ...doc }))
}

async function fetchStatsGames({ startDate, endDate }) {
  const startDateStr = moment(startDate).format("YYYY-MM-DD")
  const endDateStr = moment(endDate).format("YYYY-MM-DD")

  const url = `http://statsapi.mlb.com/api/v1/schedule/games/?sportId=1&startDate=${startDateStr}&endDate=${endDateStr}`
  console.log('fetchStatsGames', startDateStr, endDateStr, url)

  const result = await fetch(url).then(res => res.json())

  const now = new Date()

  return result.dates
               .map(date => date.games.map(doc => ({
                 game_pk: doc.gamePk.toString(),
                 updatedAt: now,
                 date: date.date,
                 gameAt: new Date(doc.gameDate),
                 ...doc
               })))
               .flat()
}


module.exports = {
  fetchGdxGames,
  fetchStatsGames,
}
