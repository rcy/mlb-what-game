const { ApolloServer, gql } = require('apollo-server');
const fetch = require('node-fetch');
const moment = require('moment');
const { fetchGdxGames, fetchStatsGames } = require('./fetchGames');
const connect = require('./mongo')

const typeDefs = gql`
  type Game {
    id: ID!
    gamePk: ID!
    link: String!
    gameType: String!
    season: String!
    gameDate: String! # date
    rescheduledFrom: String # date
    status: GameStatus!
    away: GameTeam!
    home: GameTeam!
    #    venue: Venue!
    #    contentLink: String
    isTie: Boolean
    mlbtv: String
  }

  type GameStatus {
    abstractGameState: String
    codedGameState: String
    detailedState: String
    statusCode: String
    abstractGameCode: String
    reason: String
  }

  type GameTeam {
    leagueRecord: Record!
    score: Int
    team: Team!
    probable: Pitcher
  }

  type Team {
    id: ID!
    name: String!
    link: String!
  }

  type Record {
    wins: Int!
    losses: Int!
    pct: String!
  }

  type Pitcher {
    id: ID!
    era: String!
    name: String!
    first_name: String!
    last_name: String!
    throwinghand: String!
  }

  type Query {
    fetchStatsGames(startDate: String!, endDate: String!): Int
    fetchGdxGames(date: String!): Int

    # Fetch both stats and gdx games.  Run this from cron periodically (https://www.easycron.com/user).
    fetchUpdates: String

    games(date: String): [Game]
    today: String
  }
`;

//const { statsStore } = require('./fetchGames')

async function fetchAndStoreGdxGames({ date }) {
  const docs = await fetchGdxGames({ date })
  for (let doc of docs) {
    // upsert game_pk / probable pairs so if a probable changes, we have the change history
    await mongo.db().collection('gdx_games').replaceOne({ game_pk: doc.game_pk }, doc, { upsert: true })

    await mongo.db().collection('probables').replaceOne({
      game_pk: doc.game_pk,
      side: 'away',
      'pitcher.id': doc.away_probable_pitcher ? doc.away_probable_pitcher.id : null,
    }, {
      game_pk: doc.game_pk,
      side: 'away',
      pitcher: doc.away_probable_pitcher,
      updatedAt: doc.updatedAt,
    }, { upsert: true })

    await mongo.db().collection('probables').replaceOne({
      game_pk: doc.game_pk,
      side: 'home',
      'pitcher.id': doc.home_probable_pitcher ? doc.home_probable_pitcher.id : null,
    }, {
      game_pk: doc.game_pk,
      side: 'home',
      pitcher: doc.home_probable_pitcher,
      updatedAt: doc.updatedAt,
    } , { upsert: true })

    console.log('gdx upserted', doc.game_pk)
  }
  return docs.length
}

async function fetchAndStoreStatsGames({ startDate, endDate }) {
  const games = await fetchStatsGames({ startDate, endDate })
  for (let game of games) {
    await mongo.db().collection('stats_games').replaceOne({ gamePk: game.gamePk }, game, { upsert: true })
    console.log('stats upserted', game.gamePk)
  }
  return games.length
}

function today() {
  return moment().utcOffset(-8 * 60).format("YYYY-MM-DD")
}

// Resolvers define the technique for fetching the types in the
// schema.  We'll retrieve books from the "books" array above.
const resolvers = {
  Query: {
    today: async () => today(),

    fetchUpdates: async () => {
      const startDate = today()
      await fetchAndStoreGdxGames({ startDate })
      await fetchAndStoreStatsGames({ startDate, endDate: startDate })
      return startDate
    },

    fetchStatsGames: async (doc, { startDate, endDate }) => {
      return await fetchAndStoreStatsGames({ startDate, endDate })
    },

    fetchGdxGames: async (doc, { date }) => {
      return await fetchAndStoreGdxGames({ date })
    },

    games: async (doc, { date }) => {
      return mongo.db().collection('stats_games')
                  .aggregate([
                    {
                      $match: { date }
                    },
                    {
                      $lookup: {
                        from: 'probables', localField: 'game_pk', foreignField: 'game_pk', as: 'probables',
                      }
                    },
                    {
                      $sort: { gameAt: 1 },
                    }
                  ])
                  .toArray()
    },
  },

  Game: {
    id: doc => doc.gamePk,
    away: (doc) => {
      const prob = [...doc.probables].sort((a, b) => b.updatedAt - a.updatedAt).find(prob => prob.side === 'away')
      return { probable: prob && prob.pitcher, ...doc.teams.away }
    },
    home: (doc) => {
      const prob = [...doc.probables].sort((a, b) => b.updatedAt - a.updatedAt).find(prob => prob.side === 'home')
      return { probable: prob && prob.pitcher, ...doc.teams.home }
    },
    mlbtv: (doc) => {
      return `https://www.mlb.com/tv/g${doc.gamePk}`
    },
  },

  Pitcher: {
    name: doc => `${doc.first_name} ${doc.last_name}`
  }
};

// In the most basic sense, the ApolloServer can be started
// by passing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types.
const server = new ApolloServer({ typeDefs, resolvers });

// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
let mongo
connect().then(connection => {
  mongo = connection
  console.log('mongo connection', { mongo })
  server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
  });
})
