import React, { useEffect } from 'react';
import { useQuery } from 'urql';
import moment from 'moment';

const query = `
    query GetGames($date: String) {
      games(date: $date) { 
        id
        gameDate
        mlbtv
        status {
          detailedState
          reason
        }

        home { 
          score
          probable {
            name
            era
          }
          team { name }
        }
        away { 
          score
          probable {
            name
            era
          }
          team { name }
        }
      }
    }
`

function App() {
  const [{ fetching, data }, executeQuery] = useQuery({
    query,
    variables: {
      date: moment().format("YYYY-MM-DD"),
    }
  });

  useEffect(() => {
    const timer = setInterval(() => {
      executeQuery()
    }, 10000)

    return () => {
      clearInterval(timer)
    }
  }, [])

  console.log({ fetching, data })

  if (fetching) {
    return "fetching"
  }
  if (!data) {
    return "nodata"
  }

  const { games } = data

  console.log(games)

  return (
    <table>
      <tbody>
        {games.map(game => (
          <React.Fragment key={game.id}>
            <tr>
              <td>{moment(game.gameDate).format("LLL")}</td>
              <td/>
              <td>
                <a href={game.mlbtv} target="what-game">
                  {game.status.detailedState} {game.status.reason}
                </a>
              </td>
            </tr>
            <tr>
              <td>{game.away.team.name}</td>
              <td>{game.away.score}</td>
              <td>
                {game.away.probable && game.away.probable.name} {game.away.probable && game.away.probable.era}
              </td>
            </tr>
            <tr>
              <td>{game.home.team.name}</td>
              <td>{game.home.score}</td>
              <td>
                {game.home.probable && game.home.probable.name} {game.home.probable && game.home.probable.era}
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </React.Fragment>
        ))}
      </tbody>
    </table>
  );
}

export default App;
