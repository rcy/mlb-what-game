import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider, createClient } from 'urql';

console.log(JSON.stringify(process.env))

const client = createClient({
  url: process.env.REACT_APP_GRAPHQL_API || 'http://localhost:4000',
});

ReactDOM.render(
  <Provider value={client}>
    <App />
  </Provider>
  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
